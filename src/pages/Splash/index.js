import React, {useEffect} from 'react';
import { View, Text,Image} from 'react-native';


const Splash = ({navigation}) => {
    useEffect(()=> {
        setTimeout(() => {
             navigation.navigate('WelcomeAuth')
        }, 3000)
    })
    return (
        <View style={{backgroundColor:'white'}}>
            <Image source={require('../Splash/logo.png')}style={{width:100,height:100,marginLeft:160,marginTop:300,marginBottom:5}}></Image>
            <Text style={{alignItems:'center',justifyContent:'center',marginTop:0,marginBottom:500,borderRadius:5,fontSize:20,color:'purple',
        textAlign:'center'}}>Welcome to InstaDoy</Text>
        </View>
    );
};

export default Splash;